const express = require(`express`);
const app = express(); //creating a server
const mongoose = require(`mongoose`);
const PORT = 5000;

//routers
let userRoutes = require('./routes/userRoutes')



app.use(express.urlencoded({extended:true}));
app.use(express.json());

mongoose.connect('mongodb+srv://xeniamaela:Xoden_101@cluster0.padkt.mongodb.net/s31?retryWrites=true&w=majority',
	{
		useNewUrlParser:true, 
		useUnifiedTopology: true
	}
).then(()=>console.log('Connected to database')).catch((error)=>console.log(error));

app.use("/", userRoutes);

app.listen(PORT, ()=> console.log(`Server is running at ${PORT}`));