//deals with business logic

const User = require('./../models/User.js');


//add user using anonymous function
//reqBody is just a parameter it can be named with anything
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		password: reqBody.password
	});
	return newUser.save().then((error,savedUser)=>{
		if (error) {
			//use return keyword coz there is not res.send here
			return error
		} else {
			return savedUser
		}
	});
}


module.exports.findAllUser = () => {
	return User.find({}).then((error, userList)=>{
		if (error) {
			return error
		} else {
			return userList
		}
	});
}

module.exports.findSpecificUser = (userId) => {
	//let userId = req.params.userId;

	return User.findById(userId).then((error, specificUser)=>{
		if (error) {
			return error
		} else {
			return specificUser
		}
	});
}

module.exports.updateUser = (userId,reqBody)=> {
	return User.findByIdAndUpdate(userId,reqBody,{new:true}).then((error, updatedUser)=>{
		if (error) {
			return error
		} else {
			return updatedUser
		}
	})
}

module.exports.deleteUser = (userId)=> {
	return User.findByIdAndDelete(userId).then((error, deletedUser)=>{
		if(error){
			return error
		} else {
			return ('user deleted');
		}
	});
}

