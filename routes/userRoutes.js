const express = require(`express`);
const router = express.Router();

///model
const User = require('./../models/User.js');

//controller
const userController =require('./../controllers/userControllers.js')

//routes
	//requests
		//params
		//body
		//header

//insert new user
router.post("/add-user", (req,res)=>{
/*	// console.log(req.body)

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		username: req.body.username,
		password: req.body.password
	});

	newUser.save((error,savedUser)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(`New user was saved. ${savedUser}`)
		}
	});*/

	userController.createUser(req.body).then(result=>res.send(result));
})

//retrieve all user
router.get("/retrieve-users", (req,res)=> {
/*	//model.method
	User.find({}, (error, userList)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(userList);
		}
	});*/
	userController.findAllUser().then(result=>res.send(result))
});

//retrieve specific user
router.get("/retrieve-users/:userId", (req,res)=> {
	let userId = req.params.userId;
/*
	User.findById(userId, (error, specificUser)=>{
		if (error) {
			console.log(error);
		} else {
			res.send(specificUser);
		}
	});*/
	userController.findSpecificUser(userId).then(result=>res.send(result))
});

//update a single user
router.put("/user/:userId", (req, res)=>{
	let userId = req.params.userId;

/*	User.findByIdAndUpdate(userId, req.body,{new:true}, (error, updatedUser)=>{
		if (error) {
			res.send(error);
		} else {
			res.send(updatedUser);
		}
	})*/
	userController.updateUser(userId,req.body).then(result=>res.send(result))
})

//delete user
router.delete('/delete-user/:userId', (req,res)=> {

	let userId = req.params.userId;
/*	User.findByIdAndDelete(userId, (error, deletedUser)=>{
		if(error){
			console.log(error);
		} else {
			res.send('User deleted!');
		}
	});*/
	userController.deleteUser(userId).then(result=>res.send(result));
});

module.exports = router;
